from django.contrib import admin

from star_wars_people.models import Dataset

admin.site.register(Dataset)
