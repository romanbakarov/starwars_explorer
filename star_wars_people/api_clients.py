import requests
from requests.exceptions import HTTPError


class StarWarsApiClient:
    BASE_URL = 'https://swapi.dev/api'

    def __init__(self, endpoint=None):
        if endpoint is not None:
            self.endpoint = f'/{endpoint}/'

    def get_data_from_specific_page(self, page_number=1):
        url = self.BASE_URL + self.endpoint
        try:
            response = requests.get(url, {'page': page_number})
        except HTTPError:
            return
        return response.json()

    def resolve_url_value_for_specific_key(self, url, key):
        try:
            response = requests.get(url)
            return response.json().get(key)
        except (KeyError, HTTPError):
            return None



