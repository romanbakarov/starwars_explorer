from abc import ABC, abstractmethod

from star_wars_people.api_clients import StarWarsApiClient


class StarWarsApiSerializer(ABC):
    def __init__(self, api_endpoint):
        self.api_endpoint = api_endpoint
        self.api_client = StarWarsApiClient(self.api_endpoint)

    @abstractmethod
    def serialize(self, data):
        pass

    def data_about_single_entity_generator(self, start_page=1):
        page_number = start_page
        while True:
            page = self.api_client.get_data_from_specific_page(page_number)
            for data_about_entity in page['results']:
                yield self.serialize(data_about_entity)

            if page['next'] is None:
                break

            page_number += 1


class StarWarsPeopleSerializer(StarWarsApiSerializer):
    columns_to_write = ['name', 'height', 'mass', 'hair_color', 'skin_color',
                        'eye_color', 'birth_year', 'gender', 'homeworld', 'date']

    def drop_keys(self, data):
        return {k: v for k, v in data.items() if k in self.columns_to_write}

    def serialize(self, data):
        data['date'] = data['edited'][0:10]
        data['homeworld'] = self.api_client.resolve_url_value_for_specific_key(data['homeworld'], 'name')
        data = self.drop_keys(data)
        return data
