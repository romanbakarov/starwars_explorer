import os
from abc import ABC, abstractmethod
from csv import DictWriter

import petl
from django.core.files import File

from star_wars_people.models import Dataset
from star_wars_people.serializers import StarWarsPeopleSerializer
from star_wars_people.utils import generate_unique_name


class DataWriterAndDBSaver(ABC):
    file_name = None

    def write_collected_data_to_file_and_save_to_db(self):
        self.write_collected_data_to_file()
        self.save_file_to_db_and_remove_from_disc()

    @abstractmethod
    def write_collected_data_to_file(self):
        pass

    @abstractmethod
    def save_file_to_db_and_remove_from_disc(self):
        pass


class CSVDataWriterAndDBSaver(DataWriterAndDBSaver):

    def __init__(self):
        self.file_name = generate_unique_name()
        self.serializer = StarWarsPeopleSerializer('people')

    def write_collected_data_to_file(self):
        with open(self.file_name, 'w+') as dataset_file:
            writer = DictWriter(dataset_file, self.serializer.columns_to_write)
            writer.writeheader()

            for character in self.serializer.data_about_single_entity_generator():
                writer.writerow(character)

    def save_file_to_db_and_remove_from_disc(self):
        with open(self.file_name, 'r') as dataset_file:
            Dataset.objects.create(file=File(dataset_file), file_name=self.file_name)
            os.remove(self.file_name)


# DatasetDetailView helper function
def get_data_from_csv(obj, number_of_rows):
    table = petl.fromcsv(obj.file.file)
    return petl.dicts(petl.rowslice(table, 0, number_of_rows))


# DatasetCountView helper function
def count_aggregation(obj, columns):
    table = petl.fromcsv(obj.file.file)
    aggregation = {'count': len}
    aggregated_table = petl.dicts(
        petl.aggregate(table, key=columns, aggregation=aggregation)
    )
    return aggregated_table


def get_list_of_column_names_from_csv(obj):
    table = petl.fromcsv(obj.file.file)
    return table[0]
