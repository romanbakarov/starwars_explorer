from threading import Thread

from star_wars_people.services import CSVDataWriterAndDBSaver


class FetchDataTask(Thread):
    data_fetcher = CSVDataWriterAndDBSaver()

    def run(self):
        self.data_fetcher.write_collected_data_to_file_and_save_to_db()
