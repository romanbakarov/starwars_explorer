import shutil

from django.core.files import File
from django.test import TestCase, override_settings

from ..models import Dataset

TEST_DIR = 'test_data'


class DatasetListViewTestCase(TestCase):
    @override_settings(MEDIA_ROOT=TEST_DIR)
    def setUp(self):
        self.url = ''
        with open('star_wars_people/tests/test.csv', 'r') as dataset_file:
            self.dataset = Dataset.objects.create(file=File(dataset_file), file_name='test_file')

    def tearDown(self):
        try:
            shutil.rmtree(TEST_DIR)
        except OSError:
            pass

    def test_GET(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'datasets-list.html')


class DatasetDetailViewTestCase(TestCase):
    @override_settings(MEDIA_ROOT=TEST_DIR)
    def setUp(self):
        with open('star_wars_people/tests/test.csv', 'r') as dataset_file:
            self.dataset = Dataset.objects.create(file=File(dataset_file), file_name='test_file')
        self.url = f'/dataset/{self.dataset.pk}/detail/'

    def tearDown(self):
        try:
            shutil.rmtree(TEST_DIR)
        except OSError:
            pass

    @override_settings(MEDIA_ROOT=TEST_DIR)
    def test_GET(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'dataset-detail.html')

    @override_settings(MEDIA_ROOT=TEST_DIR)
    def test_response_has_10_rows_by_default(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['table']), 10)

    @override_settings(MEDIA_ROOT=TEST_DIR)
    def test_load_more_button(self):
        response = self.client.get(self.url, {'number_of_rows': 20})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['table']), 20)

    @override_settings(MEDIA_ROOT=TEST_DIR)
    def test_view_renders_correct_data(self):
        response = self.client.get(self.url)
        response_header = response.context['table'][0].keys()
        response_row = response.context['table'][0].values()
        expected_row = [
            'Luke Skywalker', '172', '77', 'blond', 'fair', 'blue', '19BBY', 'male', 'Tatooine', '2014-12-20'
        ]
        expected_header = ['name', 'height', 'mass', 'hair_color', 'skin_color',
                           'eye_color', 'birth_year', 'gender', 'homeworld', 'date']

        self.assertEqual(set(response_header), set(expected_header))
        self.assertEqual(set(response_row), set(expected_row))


class DatasetCountViewTestCase(TestCase):
    @override_settings(MEDIA_ROOT=TEST_DIR)
    def setUp(self):
        with open('star_wars_people/tests/test.csv', 'r') as dataset_file:
            self.dataset = Dataset.objects.create(file=File(dataset_file), file_name='test_file')
        self.url = f'/dataset/{self.dataset.pk}/count/'

    def tearDown(self):
        try:
            shutil.rmtree(TEST_DIR)
        except OSError:
            pass

    @override_settings(MEDIA_ROOT=TEST_DIR)
    def test_GET(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'dataset-count.html')

    @override_settings(MEDIA_ROOT=TEST_DIR)
    def test_view_renders_correct_data(self):
        response = self.client.get(self.url)
        response_header = response.context['table'][0].keys()
        response_row = response.context['table'][0].values()
        expected_header = ['name', 'height', 'mass', 'hair_color', 'skin_color',
                           'eye_color', 'birth_year', 'gender', 'homeworld', 'date', 'count']
        expected_row = ['Ackbar', '180', '83', 'none', 'brown mottle', 'orange', '41BBY', 'male', 'Mon Cala',
                        '2014-12-20', 1]
        self.assertEqual(set(response_header), set(expected_header))
        self.assertEqual(set(response_row), set(expected_row))

    @override_settings(MEDIA_ROOT=TEST_DIR)
    def test_aggregation_for_date_columns(self):
        response = self.client.get(self.url, {'columns': 'date'})
        response_row = response.context['table'][0].values()
        expected_row = ['2014-12-20', 82]
        self.assertEqual(set(response_row), set(expected_row))

    @override_settings(MEDIA_ROOT=TEST_DIR)
    def test_incorrect_query_params(self):
        response = self.client.get(self.url, {'columns': 'qwerqadadqwe'})
        response_header = response.context['table'][0].keys()
        response_row = response.context['table'][0].values()
        expected_header = ['name', 'height', 'mass', 'hair_color', 'skin_color',
                           'eye_color', 'birth_year', 'gender', 'homeworld', 'date', 'count']
        expected_row = ['Ackbar', '180', '83', 'none', 'brown mottle', 'orange', '41BBY', 'male', 'Mon Cala',
                        '2014-12-20', 1]
        self.assertEqual(set(response_header), set(expected_header))
        self.assertEqual(set(response_row), set(expected_row))

    @override_settings(MEDIA_ROOT=TEST_DIR)
    def test_two_query_params(self):
        response = self.client.get(self.url, {'columns': ['date', 'gender']})
        response_header = response.context['table'][0].keys()
        expected_header = ['date', 'gender', 'count']
        self.assertEqual(set(response_header), set(expected_header))
