from django.urls import path

from star_wars_people.views import DatasetsListView, fetch_data, DatasetDetailView, DatasetCountView

urlpatterns = [
    path('', DatasetsListView.as_view(), name='collections-list'),
    path('fetch/', fetch_data, name='fetch'),
    path('dataset/<int:pk>/detail/', DatasetDetailView.as_view(), name='collection-detail'),
    path('dataset/<int:pk>/count/', DatasetCountView.as_view(), name='collection-count')
]
