from django.shortcuts import redirect
from django.views.generic import ListView
from django.views.generic.detail import DetailView

from star_wars_people.models import Dataset
from star_wars_people.services import get_data_from_csv, count_aggregation, get_list_of_column_names_from_csv
from star_wars_people.tasks import FetchDataTask


class DatasetsListView(ListView):
    model = Dataset
    template_name = 'datasets-list.html'
    context_object_name = 'datasets'


def fetch_data(request):
    FetchDataTask().start()
    return redirect('collections-list')


class DatasetDetailView(DetailView):
    queryset = Dataset.objects.all()
    template_name = 'dataset-detail.html'
    rendered_number_of_rows_by_default = 10

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        number_of_rows_to_render = self.get_current_number_of_rows_displayed()
        column_names = get_list_of_column_names_from_csv(self.object)
        context['table'] = get_data_from_csv(self.object, number_of_rows_to_render)
        context['columns'] = column_names
        context['number_of_rows'] = self.rendered_number_of_rows_by_default + number_of_rows_to_render
        return context

    def get_current_number_of_rows_displayed(self):
        number_of_rows = self.request.GET.get('number_of_rows', None)
        try:
            if number_of_rows is None:
                return self.rendered_number_of_rows_by_default
            return int(number_of_rows)
        except ValueError:
            return self.rendered_number_of_rows_by_default


class DatasetCountView(DetailView):
    queryset = Dataset.objects.all()
    template_name = 'dataset-count.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        column_names = get_list_of_column_names_from_csv(self.object)
        selected_columns_for_aggregation = self.get_columns_for_aggregation()
        context['table'] = count_aggregation(self.object, selected_columns_for_aggregation)
        context['columns'] = column_names
        return context

    def get_columns_for_aggregation(self):
        columns_in_rendered_csv_file = get_list_of_column_names_from_csv(self.object)
        columns_for_aggregation = self.request.GET.getlist('columns')
        if not columns_for_aggregation:
            return columns_in_rendered_csv_file

        for column in columns_for_aggregation:
            if column not in columns_in_rendered_csv_file:
                return columns_in_rendered_csv_file

        if len(columns_for_aggregation) == 1:
            return columns_for_aggregation[0]

        return columns_for_aggregation
